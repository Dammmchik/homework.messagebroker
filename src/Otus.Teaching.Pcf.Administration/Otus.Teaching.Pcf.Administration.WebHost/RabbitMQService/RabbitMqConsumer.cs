﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using Otus.Teaching.Pcf.Administration.WebHost.Models;
using Otus.Teaching.Pcf.Administration.WebHost.PromocodesService;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.WebHost.RabbitMQService
{
    public class RabbitMqConsumer : BackgroundService
    {
        private IConnection _connection;
        private IModel _channel;
        private readonly IPromocodesService _promocodesService;

        public RabbitMqConsumer(IServiceProvider serviceProvider)
        {
            _promocodesService = serviceProvider.CreateScope().ServiceProvider.GetRequiredService<IPromocodesService>();

            var factory = new ConnectionFactory { HostName = "localhost" };
            _connection = factory.CreateConnection();
            _channel = _connection.CreateModel();
            _channel.QueueDeclare(queue: "PromocodesQueue", durable: false, exclusive: false, autoDelete: false, arguments: null);
            _channel.QueueBind(queue: "PromocodesQueue", exchange: "PromocodesExchange", routingKey: "promocodes.Admin", null);
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            stoppingToken.ThrowIfCancellationRequested();

            var consumer = new EventingBasicConsumer(_channel);
            consumer.Received += async (ch, ea) =>
            {
                var content = Encoding.UTF8.GetString(ea.Body.ToArray());

                var res = JsonConvert.DeserializeObject<PromocodeMessage>(content);

                await _promocodesService.UpdateAppliedPromocodesAsync(res.PartnerManagerId.Value);

                _channel.BasicAck(ea.DeliveryTag, false);
            };

            _channel.BasicConsume("PromocodesQueue", false, consumer);

            return Task.CompletedTask;
        }

        public override void Dispose()
        {
            _channel.Close();
            _connection.Close();
            base.Dispose();
        }
    }
}
