﻿using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using System;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.WebHost.PromocodesService
{
    public class PromocodesService : IPromocodesService
    {
        private readonly IRepository<Employee> _employeeRepository;

        public PromocodesService(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }


        public async Task UpdateAppliedPromocodesAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return;

            employee.AppliedPromocodesCount++;

            await _employeeRepository.UpdateAsync(employee);
        }
    }
}
