﻿using System;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.WebHost.PromocodesService
{
    public interface IPromocodesService
    {
        Task UpdateAppliedPromocodesAsync(Guid id);
    }
}
