﻿using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;
using System;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.WebHost.Models
{
    public class PromocodeMessage
    {
        public string Code { get; set; }

        public string ServiceInfo { get; set; }

        public DateTime BeginDate { get; set; }

        public DateTime EndDate { get; set; }

        public Guid PartnerId { get; set; }

        public Guid? PartnerManagerId { get; set; }

        public Guid PreferenceId { get; set; }

        public PromocodeMessage(PromoCode promocode)
        {

            BeginDate = promocode.BeginDate;
            EndDate = promocode.EndDate;
            PartnerId = promocode.PartnerId;
            Code = promocode.Code;
            ServiceInfo = promocode.ServiceInfo;
            PartnerManagerId = promocode.PartnerManagerId;
            PreferenceId = promocode.PreferenceId;
        }
    }


}
