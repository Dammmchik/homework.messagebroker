﻿using RabbitMQ.Client;
using System.Text;
using System.Text.Json;

public class RabbitMqService : IRabbitMqService
{
	public RabbitMqService()
	{
		var factory = new ConnectionFactory() { HostName = "localhost" };
		using (var connection = factory.CreateConnection())
		using (var channel = connection.CreateModel())
		{
			channel.ExchangeDeclare("PromocodesExchange", ExchangeType.Fanout);
		}
	}

	public void SendMessage(object obj)
	{
		var message = JsonSerializer.Serialize(obj);
		SendMessage(message);
	}

	public void SendMessage(string message)
	{
		var factory = new ConnectionFactory() { HostName = "localhost" };
		using (var connection = factory.CreateConnection())
		using (var channel = connection.CreateModel())
		{
			var body = Encoding.UTF8.GetBytes(message);

			channel.BasicPublish(exchange: "PromocodesExchange",
						   routingKey: "",
						   basicProperties: null,
						   body: body);
		}
	}
}